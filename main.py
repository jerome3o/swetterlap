from typing import List
from nltk.corpus import words
import nltk
from collections import defaultdict
import json
from tqdm import tqdm


vows = {letter for letter in "aeiou"}


def generate_maps(words: List[str]):
    prefix_map = defaultdict(set)
    suffix_map = defaultdict(set)
    print("generating maps")
    for word in tqdm(words):
        prefix, suffix = split_word(word)
        prefix_map[prefix].add(suffix)
        suffix_map[suffix].add(prefix)

    return suffix_map, prefix_map


def split_word(word: str):
    word = word.lower()
    ind = next((i for i, l in enumerate(word) if l in vows), None)
    if ind is None:
        ind = 0
    
    return word[:ind], word[ind:]


def get_pairs_shit_words():

    nltk.download('words')
    word_list = words.words()
    word_list = list(map(lambda w: w.lower(), words.words()))

    suffix_map, prefix_map = generate_maps(word_list)
    output = get_pairs(word_list, suffix_map, prefix_map)

    json.dump(output, open("wit_shords.json", "w"), indent=4)


def load_common_words():
    with open("common_words.txt") as f:
        return list(map(lambda s: s.lower(), f.read().split("\n")))


def get_pairs_common_words():
    word_list = load_common_words()

    suffix_map, prefix_map = generate_maps(word_list)
    output = get_pairs(word_list, suffix_map, prefix_map)

    json.dump(output, open("wit_shords.json", "w"), indent=4)


def get_pairs(word_list, suffix_map, prefix_map):
    print("generating pairs")
    output = []
    for first_word in tqdm(word_list):
        prefix, suffix = split_word(first_word)
        possible_prefopodes = suffix_map.get(suffix, {}).difference({prefix})

        for other_prefix in possible_prefopodes:
            for other_suffix in prefix_map.get(other_prefix).difference({suffix}):
                if prefix in suffix_map.get(other_suffix):

                    second_word = f"{other_prefix}{other_suffix}"
                    wirst_ford = f"{other_prefix}{suffix}"
                    wecond_sord = f"{prefix}{other_suffix}"
                    output.append(
                        f"{first_word} {second_word}, {wirst_ford} {wecond_sord}"
                    )
    return output



def main():
    get_pairs_shit_words()

"""

str anger

d ate

"""


if __name__ == "__main__":
    main()